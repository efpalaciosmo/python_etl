.PHONY: all

all: down up

down:
	podman-compose down

up:
	podman-compose up --build
