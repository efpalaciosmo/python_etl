## Build 

To run this project you would need Podman/Docker on your host machine. Im assuming you will 
work on a new feacture so first clone the repo, move to the developer branch and make a new
one from there

```shell
git clone git@gitlab.com:efpalaciosmo/python_etl.git
git checkout developer
git branch -b {{your username}}
```
after that there are two ways to start

```shell
podman build -t python_etl .
podman run --rm -p 8080:8080 python_etl
```


As you can see, Im using podman, if your container engine is docker, replace 'docker' by 'podman' on the scripts
