USE master

IF OBJECT_ID('OrderItems', 'U') IS NULL
create table OrderItems
(
    id                  int identity
        primary key,
    orderItemsId        int                                                 not null,
    orderId             int,
    name                nvarchar(max),
    productId           int,
    variationId         int,
    quantity            int,
    taxClass            nvarchar(255),
    subtotal            nvarchar(255),
    total               nvarchar(255),
    meta_data_id        int,
    [key]               nvarchar(255),
    value               nvarchar(255),
    displayKey          nvarchar(255),
    displayValue        nvarchar(255),
    display_key         nvarchar(255),
    display_value       nvarchar(255),
    sku                 nvarchar(255),
    price               int,
    image               nvarchar(255),
    parent_name         nvarchar(255),
    createdAt           datetime default ((getdate() AT TIME ZONE 'UTC') AT TIME ZONE
                                          'Eastern Standard Time (Mexico)') not null,
    updatedAt           datetime,
    dateCreated         datetime,
    dateModified        datetime,
    aceptacion_terminos varchar(255)
)
GO

IF OBJECT_ID('Orders', 'U') IS NULL
create table Orders
(
    id                int identity
        primary key,
    orderId           int,
    status            nvarchar(255),
    currency          nvarchar(255),
    pricesIncludeTax  bit,
    dateCreated       datetime,
    dateModified      datetime,
    discountTotal     nvarchar(255),
    discountTax       nvarchar(255),
    shippingTotal     nvarchar(255),
    shippingTax       nvarchar(255),
    total             nvarchar(255),
    customerId        int,
    firstName         nvarchar(255),
    lastName          nvarchar(255),
    address1          nvarchar(1024),
    address2          nvarchar(1024),
    city              nvarchar(255),
    state             nvarchar(255),
    country           nvarchar(255),
    email             nvarchar(255),
    phone             nvarchar(255),
    shippingFirstFame nvarchar(255),
    shippingLastName  nvarchar(255),
    shippingAddress1  nvarchar(1024),
    shippingAddress2  nvarchar(1024),
    shippingCity      nvarchar(255),
    shippingState     nvarchar(255),
    createdAt         datetime default ((getdate() AT TIME ZONE 'UTC') AT TIME ZONE 'Eastern Standard Time (Mexico)'),
    updatedAt         datetime,
    budget            decimal(12, 2),
    latitude          varchar(255),
    longitude         varchar(255)
)
GO

IF OBJECT_ID('Products', 'U') IS NULL
create table Products
(
    id                 int identity
        primary key,
    productId          int,
    name               nvarchar(255),
    slug               nvarchar(255),
    permaLink          nvarchar(255),
    dateCreated        datetimeoffset,
    dateModified       datetimeoffset,
    type               nvarchar(255),
    status             nvarchar(255),
    featured           bit,
    catalog_visibility nvarchar(255),
    description        nvarchar(255),
    short_description  nvarchar(max),
    sku                nvarchar(255),
    price              nvarchar(255),
    total_sales        int,
    createdAt          datetimeoffset,
    updatedAt          datetimeoffset
)
GO