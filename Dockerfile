FROM python:3.11-slim-bookworm

RUN apt-get update && apt install curl -y
RUN curl https://packages.microsoft.com/keys/microsoft.asc | tee /etc/apt/trusted.gpg.d/microsoft.asc
RUN curl https://packages.microsoft.com/config/debian/11/prod.list | tee /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update -y
RUN ACCEPT_EULA=Y apt-get install unixodbc msodbcsql17 -y
RUN apt-get clean -y

# Set the working directory to /code
WORKDIR /code

RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile 


# Copy the current directory contents into the container at /code
COPY ./requirements.txt /code/requirements.txt

# UPGRADE pip3
RUN pip3 install --upgrade pip
RUN pip3 install -r ./requirements.txt

# Copy the src directory into the container at /code/src
COPY ./src /code/src

ENV PYTHONUNBUFFERED=1
ENV TZ=America/Bogota
# Run uvicorn when the container launches
CMD ["python", "-u", "-m", "src.main"]
