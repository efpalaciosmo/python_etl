from sqlalchemy import text
from src.config.database import connect

from src.helper import get_date
from src.services.build_response import buildResponse

async def person():
    """
    This functions get the Person data
    """
    try:
        sql = text("""
            SELECT
                *
            FROM
                Person.Person
            """)
        engine = await connect()
        if engine is None:
            return buildResponse(500, 'Error connection to database', get_date())
        
        data = []
        with engine.connect() as connection:
            result = connection.execute(sql)
            connection.commit()
            data = result.all()
        return data
    except Exception as e:
        return buildResponse(500, f'Internal error server {e}', get_date())

async def state_province():
    """
    This functions get the Customer data
    """
    try:
        sql = text("""
            SELECT
                *
            FROM
                Person.StateProvince
            """)
        engine = await connect()
        if engine is None:
            return buildResponse(500, 'Error connection to database', get_date())
        
        data = []
        with engine.connect() as connection:
            result = connection.execute(sql)
            connection.commit()
            data = result.all()
        return data
    except Exception as e:
        return buildResponse(500, f'Internal error server {e}', get_date())


async def address():
    """
    This functions get the Address data
    """
    try:
        sql = text("""
            SELECT
                *
            FROM
                Person.Address
            """)
        engine = await connect()
        if engine is None:
            return buildResponse(500, 'Error connection to database', get_date())
        
        data = []
        with engine.connect() as connection:
            result = connection.execute(sql)
            connection.commit()
            data = result.all()
        return data
    except Exception as e:
        return buildResponse(500, f'Internal error server {e}', get_date())
