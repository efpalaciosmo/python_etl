from sqlalchemy import text
from src.config.database import connect

from src.helper import get_date
from src.services.build_response import buildResponse

async def order_header():
    """
    This functions get the SalesOrderHeader table
    """
    try:
        sql = text("""
            SELECT
                *
            FROM
                Sales.SalesOrderHeader
            """)
        engine = await connect()
        if engine is None:
            return buildResponse(500, 'Error connection to database', get_date())
        
        data = []
        with engine.connect() as connection:
            result = connection.execute(sql)
            connection.commit()
            data = result.all()
        return data
    except Exception as e:
        return buildResponse(500, f'Internal error server {e}', get_date())


async def order_detail():
    """
    This functions get the SalesOrderDetail date
    """
    try:
        sql = text("""
            SELECT
                *
            FROM
                Sales.SalesOrderDetail
            """)
        engine = await connect()
        if engine is None:
            return buildResponse(500, 'Error connection to database', get_date())
        
        data = []
        with engine.connect() as connection:
            result = connection.execute(sql)
            connection.commit()
            data = result.all()
        return data
    except Exception as e:
        return buildResponse(500, f'Internal error server {e}', get_date())

async def customer():
    """
    This functions get the Customer date
    """
    try:
        sql = text("""
            SELECT
                *
            FROM
                Sales.Customer
            """)
        engine = await connect()
        if engine is None:
            return buildResponse(500, 'Error connection to database', get_date())
        
        data = []
        with engine.connect() as connection:
            result = connection.execute(sql)
            connection.commit()
            data = result.all()
        return data
    except Exception as e:
        return buildResponse(500, f'Internal error server {e}', get_date())
