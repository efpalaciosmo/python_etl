from dataclasses import dataclass
import dataclasses

@dataclass
class ResponseEntity:
    """Class to model any response of the service"""
    status: int
    message: str
    date: str

    def to_json(self, include_null=False) -> dict:
        return dataclasses.asdict(
                self,
                dict_factory= lambda fields : {
                    key: value
                    for (key, value) in fields
                    if value is not None or include_null
                    }
                )

def buildResponse(status: int, message: str, date: str) -> ResponseEntity:
    return ResponseEntity(status, message, date)
