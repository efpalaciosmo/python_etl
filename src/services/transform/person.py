from src.services.build_response import ResponseEntity
import src.services.extract.person as personRepository

async def transform_customer():
    data = await personRepository.person()
    if isinstance(data, ResponseEntity):
        return data
    # transformations here
    return data

async def transform_address():
    data = await personRepository.address()
    if isinstance(data, ResponseEntity):
        return data
    # transformations here
    return data

async def transform_state_province():
    data = await personRepository.state_province()
    if isinstance(data, ResponseEntity):
        return data
    # transformations here
    return data
