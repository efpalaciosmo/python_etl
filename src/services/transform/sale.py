from src.services.build_response import ResponseEntity
import src.services.extract.sale as saleRepository

async def transform_customer():
    customer = await saleRepository.customer()
    if isinstance(customer, ResponseEntity):
        return customer
    # transformations here
    return customer

async def transform_order_header():
    order_header = await saleRepository.order_header()
    if isinstance(order_header, ResponseEntity):
        return order_header
    # transformations here
    return order_header

async def transform_order_detail():
    order_detail = await saleRepository.order_detail()
    if isinstance(order_detail, ResponseEntity):
        return order_detail
    # transformations here
    return order_detail
