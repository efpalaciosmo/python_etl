import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from dotenv import load_dotenv

load_dotenv()

sender_email = os.getenv('EMAIL_SENDER')
receiver_email = os.getenv('EMAIL_RECEIVER')


# Create the plain-text and HTML version of your message
def send_email() -> None:
    """Function used to send emails"""
    message = Mail(
        from_email=sender_email,
        to_emails=receiver_email,
        subject='Sending with Twilio SendGrid is Fun',
        html_content='<strong>and easy to do anywhere, even with Python</strong>')
    try:
        sg = SendGridAPIClient(os.getenv('SENDGRID_API_KEY'))
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e)


send_email()
