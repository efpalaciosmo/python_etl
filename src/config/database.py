from sqlalchemy import create_engine
import sqlalchemy as sa

async def connect():
    try:
        connection_string = (
            'Driver={ODBC Driver 17 for SQL Server};'
            'Server=localhost;'
            'Database=AdventureWorks2017;'
            'UID=sa;'
            'PWD=2025;'
            'Trusted_Connection=no;'
        )

        connection_url = sa.engine.URL.create(
            "mssql+pyodbc", 
            query=dict(odbc_connect=connection_string)
        )
        engine = create_engine(connection_url, fast_executemany=True)
        return engine
    except sa.exc.OperationalError as e:
        print(f"OperationalError: {e}")
        return None, str(e)
    except Exception as e:
        print(f"An unexpected error occurred: {e}")
        return None
