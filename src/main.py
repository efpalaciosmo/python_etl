from fastapi import FastAPI
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.triggers.cron import CronTrigger
import asyncio

from src.services.schedule import run_scheduler

async def main():
    scheduler = AsyncIOScheduler()
    trigger = CronTrigger(
            year="*", month="*", day="*", hour="6", minute="0", second="0"
    )
    scheduler.add_job(
            run_scheduler,
            trigger=trigger,
            id='Run tasks every day at 6am'
        )
    scheduler.start()

if __name__ == '__main__':
    try:
        asyncio.run(main())
    except (KeyboardInterrupt, SystemExit):
        pass

    app = FastAPI()
    app.add_event_handler("startup", main)

    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000, log_level="info")
