import json
from datetime import datetime
from zoneinfo import ZoneInfo

def get_first_element(lst):
    return json.dumps(lst[0]) if lst else None
    
def get_second_element(lst):
    return json.dumps(lst[0]) if lst else None

def get_third_element(lst):
    return json.dumps(lst[2]) if lst else None

def get_first_value(lst):
    return lst[0]

def get_date() -> str:
    date = datetime.now(tz=ZoneInfo("America/Bogota"))
    return date.strftime('%Y-%m-%d %H:%M:%S')